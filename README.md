# Photomilc

## Description 
**Photomilc** is a set of Python/Astropy tools for stellar photometry with an interchangeable lens camera. It is not a self-contained photometry software with a graphical user interface. The main script calls functions from a module called *photomilc_tools* which implements some "high level" functions for photometry, namely:
* loading of a FITS file
* conversion from CIE XYZ colorimetric system to standard Johnson-Cousins photometric system (see also the *JohnsonCousins* folder for establishing the conversion coefficients)
* global and local statistics, histogram
* trend (including gradient) removal, with background noise identification
* source detection and extraction, using thresholding and binarization
* source measurement, and PSF identification
* querying Simbad for star equatorial coordinates and magnitude
* differential photometry, with atmospheric extinction calibration

The main script *process_raw_fits.py* has to be adapted according to your local image acquisition conditions (pre-calibration, crop of ROI, level for sigma-clipping, guess FWHM, geographical coordinates of observatory, and the most important: the star name identification if you do not perform blind astrometry).

## Requirements
* Python 3
* Numpy
* Scipy
* Matplotlib
* Scikit-image
* Astropy
* Astroquery

## Usage
The main script takes as argument the name of a FITS file:
```
python3 process_raw_fits.py [-i] image_file.fits
```
If the *-i* option is enabled, it just shows the histogram with some basic statistics (min, max, mean, standard deviation).

## Example
A complete data reduction process is described in *doc/rapport_photometrie.pdf* (sorry, in french).
