# -*- coding: utf-8 -*-
"""
Classes and functions for stellar photometry with interchangeable lens camera
"""
import datetime
import numpy as np
import scipy.ndimage
import scipy.stats
import matplotlib.pyplot as plt
import skimage.draw
import skimage.exposure
import skimage.measure
import skimage.metrics
import skimage.morphology
import astropy.convolution
import astropy.coordinates
import astropy.io.fits as fits
import astropy.modeling
import astropy.stats
import astropy.time
import mini_star_db

#
# Mathematical utility functions
#
def snr(a):
   """
   Compute the peak signal-over-noise ratio of a numpy.ndarray
   """
   return 20*np.log10(a.max()/a.std())

def flux2mag(x):
   """
   Compute the magnitude from the flux
   """
   return -2.5*np.log10(x)

def mag2flux(x):
   """
   Compute the flux from the magnitude, with arbitrary reference
   """
   return 10**(-x/2.5)

def is_dst(t):
   """
   Return True if daylight saving time applies for date t, along CET/CEST rules

   Arguments:
   ----------
   t: an astropy.time.Time object
   
   Return:
   -------
   a bool value, True if DST applies at date/time t
   """
   dt = t.to_datetime()
   if dt.month < 3 or dt.month > 10:
      return False
   elif dt.month > 3 and dt.month < 10:
      return True
   elif dt.month == 3 or dt.month == 10:
      # look for last sunday of march or october
      for k in range(7):
         dtk = datetime.datetime(dt.year, dt.month, 31-k)
         if dtk.weekday() == 6:
            last_sunday_dt = dtk
            break
      if dt.month == 3:
         return dt.day >= last_sunday_dt.day
      else:
         return dt.day < last_sunday_dt.day

def confidence_interval(x, sigma, alpha, x_reg):
   """
   Compute the confidence interval of linear regression
   y = a * x + b + random_error
   random_error has a zero mean and a known standard deviation.

   Arguments:
   ----------
   x    : the value of the independent variable
   sigma: the standard deviation on random_error, and thus on y
   alpha: a fraction [0,1] such that 1-alpha is the confidence level
   x_reg: an array of x values used for regression
   
   Return:
   -------
   the confidence interval half-width w, such that
   [m - w, m + w] is the confidence interval around a mean value m.
   """
   n = x_reg.size
   student = scipy.stats.t(df=n-2)
   t_alpha = student.ppf(1-alpha/2)
   x_mean = x_reg.mean()
   x_sxx = n*x_reg.var()
   return t_alpha * sigma * np.sqrt(1.0/n + (x-x_mean)**2/x_sxx)

#
# FITS file handling, with basic colorimetric encoding
#
def read_image(filename, show_info=False):
   """
   Read FITS file, converting to Johnson V band if needed

   Arguments:
   ----------
   filename : FITS file name to open
   show_info: a bool value, True if print
   
   Return:
   -------
   Return the header and the datas as a numpy.ndarray
   """
   # Open file
   hdu = fits.open(filename)
   hdr = hdu[0].header
   if show_info:
      print(hdu.info())
      for k, v in hdr.items():
         if k != "COMMENT":
            print(k, v)
   # Get datas
   a = hdu[0].data
   if a.shape[0] == 3:
      # this is tri-band Z,Y,X => re-order it
      a0 = np.moveaxis(a, 0, -1)
      a0 = a0[:, :, [2,1,0]]   # X, then Y, then Z
      # XYZ to Johnson V
      k = [-0.32356639, 1.22064585, 0.01465643]
      a = k[0]*a0[:,:,0] + k[1]*a0[:,:,1] + k[2]*a0[:,:,2]
   # Dump datas
   if show_info:
      print("dtype = {}, shape = {}, size = {:6.2f} Mb" \
         .format(a.dtype, a.shape, a.nbytes/1048576))
      print("min = {:5.3f}, max = {:5.3f}, mean = {:5.3f},"
            " std = {:5.3f} snr = {:5.3f} dB" \
         .format(a.min(), a.max(), a.mean(), a.std(), snr(a)))
   hdu.close()
   return hdr, a

def save_image(filename, data, orig_hdr, as_int=False):
   """
   Save the array data into a FITS file, with name filename.
   Some fields from original header orig_hdr are copied into the new file.

   Arguments:
   ----------
   filename: FITS file name to create or overwrie
   data    : a numpy.ndarrayn, the image to save
   orig_hdr: a FITS header, which fields are to be saved into the file
   as_int  : convert to int
   """
   if data.dtype == np.float64:
      data = np.copy(data)
      # clamp negative values
      data[data < 0] = 0.0
      if as_int:
         # normalize
         data = data / data.max()
         data = skimage.util.img_as_uint(data)
   _FIELDS_COPY = "DATE-OBS", "ISO", "INSTRUME", "APERTURE", "FOCUS", "EXPTIME"
   hdu = fits.PrimaryHDU(data)
   for f in _FIELDS_COPY:
      if f in orig_hdr:
         hdu.header[f] = orig_hdr[f]
   hdul = fits.HDUList([hdu])
   hdul.writeto(filename, overwrite=True)

def histogram(a, nbins):
   """
   Compute histogram with nbins, accepting both float or integer data types

   Arguments:
   ----------
   a    : a numpy.ndarray
   nbins: the number of bins of the histogram
   
   Return:
   -------
   A tuple (ha, ba), with ha the histogram values, and ba the bounds
   of the histogram bins
   """
   if a.dtype == np.float64:
      ha, ba = skimage.exposure.histogram(a, nbins=nbins)
   else:
      # use numpy.histogram to make it work for integer
      ha, ba = np.histogram(a, bins=nbins)
      # get the center of bins, like exposure.histogram
      ba = 0.5*(ba[:-1]+ba[1:])
   return ha, ba

#
# Trend evaluation and removal functions
#
def eval_trend(a, fraction, nsigma):
   """
   Evaluate the trend across the image by computing statistics on five
   ROIs taken at the four corner and at the center

   Arguments:
   ----------
   a       : a numpy.ndarray, the image to analyze
   fraction: the fraction of the image dimension to define the size of ROI
   nsigma  : the number of standard deviation for sigma-clipping
   """
   # Sigma clip, to keep only the noise
   a1 = np.copy(a)
   a_med = np.median(a1)
   a_std = a1.std()
   a1[a1 > a_med+nsigma*a_std] = a_med
   ifl = lambda x: int(np.floor(x))
   aroi = [
      a1[:ifl(a.shape[0]*fraction), :ifl(a.shape[1]*fraction)],
      a1[:ifl(a.shape[0]*fraction), -ifl(a.shape[1]*fraction):],
      a1[-ifl(a.shape[0]*fraction):, :ifl(a.shape[1]*fraction)],
      a1[-ifl(a.shape[0]*fraction):, -ifl(a.shape[1]*fraction):],
      a1[ifl(a.shape[0]*(0.5-fraction)):ifl(a.shape[0]*(0.5+fraction)),
        ifl(a.shape[1]*(0.5-fraction)):ifl(a.shape[1]*(0.5+fraction))]
   ]
   for i, ai in enumerate(aroi):
      print("sample no. {} mean = {:5.0f} sigma = {:5.0f}"
         .format(i, ai.mean(), ai.std()))

def remove_gradient1(a, show_info=False):
   """
   Remove the gradient by fitting a plane equation

   Arguments:
   ----------
   a        : a numpy.ndarray, the image to detrend
   show_info: a bool value, True for print
   
   Return:
   -------
   The detrended image
   """
   x, y = np.meshgrid(np.arange(a.shape[1]), np.arange(a.shape[0]))
   mdl_init = astropy.modeling.functional_models.Planar2D()
   fitter = astropy.modeling.fitting.LevMarLSQFitter()
   mdl = fitter(mdl_init, x, y, a)
   mdl_xy = mdl(x, y)
   resid = np.sqrt(skimage.metrics.mean_squared_error(a, mdl_xy))
   if show_info:
      print("Plane equation of deterministic background:\n"
            "sx = {:8.3f}, sy = {:8.3f}, c = {:8.3f}, RMSE = {:8.3f}"
         .format(mdl.slope_x.value, mdl.slope_y.value,
                 mdl.intercept.value, resid))
   return a - mdl_xy

def remove_gradient2(a, nsigma, show_info=False):
   """
   Single image pre-calibration. Remove background noise and correct for
   the low spatial frequency component of the PRNU
   (Pixel Response Non Uniformity)

   Arguments:
   ----------
   a        : a numpy.ndarray, the image to detrend
   nsigma  : the number of standard deviation for sigma-clipping

   Return:
   -------
   The image corrected for trend and PRNU
   """
   # Neighborhood size for filtering operations
   msize = 20
   # Low-pass filter the noise
   a_filt = scipy.ndimage.median_filter(a, size=msize)
   # Identify and remove deterministic background
   x, y = np.meshgrid(np.arange(a.shape[1]), np.arange(a.shape[0]))
   mdl_init = astropy.modeling.polynomial.Polynomial2D(2,
      x_domain=(x[0,:].min(), x[0,:].max()),
      y_domain=(y[:,0].min(), y[:,0].max()))
   fitter = astropy.modeling.fitting.LinearLSQFitter()
   x_decim = x[::msize, ::msize]
   y_decim = y[::msize, ::msize]
   a_decim = a_filt[::msize, ::msize]
   mdl = fitter(mdl_init, x_decim, y_decim, a_decim)
   mdl_xy = mdl(x, y)
   resid = np.sqrt(skimage.metrics.mean_squared_error(a_filt, mdl_xy))
   if show_info:
      print("Deterministic background estimation, RMSE = {:8.3f}"
         .format(resid))
   # Sigma clip, to keep only the noise
   a1 = np.copy(a)
   a_med = np.median(a1)
   a_std = a1.std()
   a1[a1 > nsigma*a_std] = a_med
   # Detrend the noise
   a_noise = a1 - mdl_xy
   if show_info:
      print("detrended noise:\nmin = {:5.3f}, max = {:5.3f}, "
            "mean = {:5.3f}, std = {:5.3f}" \
         .format(a_noise.min(), a_noise.max(),
                 a_noise.mean(), a_noise.std()))
   # Try to Identify and correct for PRNU by measuring noise std dev
   print("Compute local standard deviation "
         "within neighborhood {:2d}x{:2d}..."
      .format(msize, msize))
   loc_std = scipy.ndimage.generic_filter(a_noise, np.std, size=msize)
   if show_info:
      print("local stdev:\nmin = {:5.3f}, max = {:5.3f}, "
         "mean = {:5.3f}, std = {:5.3f}" \
         .format(loc_std.min(), loc_std.max(),
                 loc_std.mean(), loc_std.std()))
   mdl_init2 = astropy.modeling.polynomial.Polynomial2D(2,
      x_domain=(x[0,:].min(), x[0,:].max()),
      y_domain=(y[:,0].min(), y[:,0].max()))
   fitter2 = astropy.modeling.fitting.LinearLSQFitter()
   x_decim = x[::msize, ::msize]
   y_decim = y[::msize, ::msize]
   a_decim = loc_std[::msize, ::msize]
   mdl2 = fitter(mdl_init2, x_decim, y_decim, a_decim)
   mdl2_xy = mdl2(x, y)
   sigma_max = mdl2_xy.max()
   resid2 = np.sqrt(skimage.metrics.mean_squared_error(loc_std, mdl2_xy))
   if show_info:
      print("PRNU estimation, RMSE = {:8.3f}".format(resid2))
   # Plot the background and local stdev with fitted models
   plt.figure(9)
   plt.subplot(1,2,1)
   plt.imshow(a_filt, cmap="RdYlBu_r", origin="lower")
   plt.contour(x, y, mdl_xy, 10)
   plt.title("Deterministic background")
   plt.subplot(1,2,2)
   plt.imshow(loc_std, cmap="RdYlBu_r", origin="lower")
   plt.contour(x, y, mdl2_xy, 10)
   plt.title("Local standard deviation")
   #plt.show()
   return sigma_max * (a - mdl_xy) / mdl2_xy

#
# Source (star) detection, extraction, and management
#
def fit_gaussian2D(a, guess_fwhm):
   """
   Fit a radial symetric 2D Gaussian to an image

   Arguments:
   ----------
   a         : a numpy.ndarray
   guess_fwhm: a guess value for the FwHM, to initialize the fitting algo

   Return:
   -------
   Return the fitted model, and the RMSE
   """
   x, y = np.meshgrid(np.arange(a.shape[1]), np.arange(a.shape[0]))
   # initialize with centroid, maximum
   moments = skimage.measure.moments(a, order=1)
   mdl_init = astropy.modeling.functional_models.Gaussian2D(
      amplitude = a.max(),
      x_mean = moments[0,1] / moments[0,0],
      y_mean = moments[1,0] / moments[0,0],
      x_stddev = guess_fwhm * astropy.stats.gaussian_fwhm_to_sigma,
      theta = 0,
      fixed= { "theta": True },
      tied={ "y_stddev": lambda m: m.x_stddev })
   fitter = astropy.modeling.fitting.LevMarLSQFitter()
   mdl = fitter(mdl_init, x, y, a)
   mdl_xy = mdl(x, y)
   RMSE = np.sqrt(skimage.metrics.mean_squared_error(a, mdl_xy))
   return mdl, RMSE

class Source:
   """
   A source is a square-shaped Region-Of-Interest (ROI) in an image defined by
   the coordinates of its centroid, and its diameter, i.e. the side length
   of the square around the centroid
   """
   def __init__(self, img, centroid, diameter):
      self._image = img   # link to original image
      self._centroid = centroid  # coords (x,y) of centroid
      self._diameter = diameter  # diameter of squared-shape ROI
      self._roi = self._image[   # the region of interest
         centroid[1] - diameter//2 : centroid[1] + diameter//2 +1,
         centroid[0] - diameter//2 : centroid[0] + diameter//2 +1
      ]
      self._moments = skimage.measure.moments(self._roi, order=1)
      self._name = ""     # the name of the source (or star)
      self._ref_mag = 1e5 # the reference visual apparent magnitude
      self._var = False   # a flag for marking the variable stars
      self._air_mass = 1  # the air mass during capture
      if self._roi.size > 0:
         self._gaussian, self._RMSE = fit_gaussian2D(self._roi, 3)
      else:
         self._gaussian = None
         self._RMSE = 0
   @property
   def image(self):
      return self._image
   @property
   def centroid(self):
      return self._centroid
   @property
   def moments(self):
      return self._moments
   @property
   def roi(self):
      return self._roi
   @property
   def name(self):
      return self._name
   @name.setter
   def name(self, n):
      """
      Set the name of the star and update its reference magnitude,
      get it from the star data base
      """
      self._name = n
      stardb = mini_star_db.StarDB()
      q = stardb.query(self._name)
      if q and q["FLUX_V"] != "--":
         self._ref_mag = float(q["FLUX_V"])
   @property
   def ref_mag(self):
      return self._ref_mag
   @ref_mag.setter
   def ref_mag(self, m):
      self._ref_mag = m
   @property
   def var(self):
      return self._var
   @var.setter
   def var(self, v):
      self._var = v
   @property
   def air_mass(self):
      return self._air_mass
   @air_mass.setter
   def air_mass(self, am):
      self._air_mass = am
   @property
   def diameter(self):
      return self._diameter
   @property
   def gaussian(self):
      return self._gaussian
   @property
   def RMSE(self):
      return self._RMSE
   def set_name_ref_mag(self, name, rm, var=False):
      """
      Set simultaneously the name, the magnitude and the variable status
      """
      self._name = name
      self._ref_mag = rm
      self._var = var

def source_detection(a, fwhm, nsig, nmax):
   """
   Source detection & extraction

   Arguments:
   ----------
   a   : a numpy.ndarray, the image
   fwhm: the assumed full width at half maximum of a source
   nsig: the number of noise standard deviation to look for the signal
   nmax: the maximum number of sources to keep

   Return:
   -------
   A list of Sources, ordered by decreasing value
   """
   # thresholding based on standard deviation
   krnl = astropy.convolution.Gaussian2DKernel(
      x_stddev = fwhm*astropy.stats.gaussian_fwhm_to_sigma)
   a_filt = astropy.convolution.convolve(a, krnl, normalize_kernel=True)
   a_thr = np.zeros_like(a, dtype=np.uint8)
   a_thr[a_filt >= nsig * a_filt.std()] = 1

   # source (star) extraction
   a_lab, num_lab = skimage.morphology.label(a_thr, return_num=True)
   print("{:3d} sources detected at {:5.1f} sigma".format(num_lab, nsig))
   x, y = np.meshgrid(np.arange(a.shape[1]), np.arange(a.shape[0]))
   centroids = []
   max_extent = 0
   for ilab in range(1, num_lab):
      # get extent of labeled region
      xemin, xemax, yemin, yemax = \
         x[a_lab == ilab].min(), \
         x[a_lab == ilab].max(), \
         y[a_lab == ilab].min(), \
         y[a_lab == ilab].max()
      # measure raw moments in original image
      moments = skimage.measure.moments(a[yemin:yemax+1, xemin:xemax+1],
         order=1)
      if moments[0,0] != 0.0:
         # compute centroid in original image
         xc = np.floor(xemin + moments[0,1] / moments[0,0])
         yc = np.floor(yemin + moments[1,0] / moments[0,0])
         centroids.append((int(xc), int(yc)))
         # update max radius
         max_extent = np.max([max_extent, xemax-xemin, yemax-yemin])
   print("max diameter of source =", max_extent)

   # Build the final list of source
   sources = [ Source(a, c, max_extent) for c in centroids ]
   sources.sort(key = lambda src: src.moments[0,0], reverse=True)
   if len(sources) > nmax:
      sources = sources[:nmax]
   # Dump extracted sources
   for i, src in enumerate(sources):
      print("source {:2d}, coords = ({:4d}, {:4d}), value = {:6.0f}, "
            "FWHM = {:6.1f}, RMSE = {:6.1f}"
            .format(i, src.centroid[0], src.centroid[1], src.moments[0,0],
            src.gaussian.x_fwhm, src.RMSE))
   return sources

def plot_sources(sources):
   """
   Plot a list of sources on an existing figure

   Arguments:
   ----------
   sources: a list of Source objects
   """
   # an RGBA image to display on top of the existing figure
   loc_img = np.zeros((sources[0].image.shape[0], sources[0].image.shape[1], 4),
                      dtype=np.uint8)
   for src in sources:
      # draw a circle
      drw_r, drw_c = skimage.draw.circle_perimeter(
         src.centroid[1], src.centroid[0], src.diameter)
      drw_r1, drw_c1 = skimage.draw.circle_perimeter(
         src.centroid[1], src.centroid[0], src.diameter+1)
      drw_r = np.hstack((drw_r, drw_r1))
      drw_c = np.hstack((drw_c, drw_c1))
      # draw in red (variable star) or green (reference star)
      loc_img[drw_r, drw_c, 0 if src.var else 1] = 255
      # alpha channel -> opaque
      loc_img[drw_r, drw_c, 3] = 255 
   plt.imshow(loc_img, origin="lower")
   # display star names
   for src in sources:
      plt.text(src.centroid[0]*1.02, src.centroid[1]*1.02, src.name,
         color = "r" if src.var else "g")

class AltAzCoords:
   """
   Altazimutal coordinates
   """
   def __init__(self, latitude, longitude, time_offset, date_time):
      """
      Initialize the reference frame with geographical coordinates,
      time zone offset and date/time of observation.
      Daylight saving time is automatically computed.

      Arguments:
      ----------
      latitude   : a string, for example "46:4:0 degrees"
      longitude  : a string, for example "4:2:0 degrees", positive east
      time_offset: offset in hours, east of greenwich, CET = +1
      date_time  : a string, for example "2017-04-23T02:40:01",
                   usually the DATE-OBS field of a FITS file
      """
      # first, specify the observation site
      latitude  = astropy.coordinates.Angle(latitude)
      longitude = astropy.coordinates.Angle(longitude)
      location = astropy.coordinates.EarthLocation(lon=longitude, lat=latitude)
      # get time of observation. Warning! Must convert to UTC!
      obstime = astropy.time.Time(date_time)
      time_offset += 1 if is_dst(obstime) else 0
      obstime_UTC = obstime \
         + astropy.time.TimeDelta(-3600*time_offset, format="sec")
      self._frame = astropy.coordinates.AltAz(obstime=obstime_UTC, location=location)
      # open the star database
      self._stardb = mini_star_db.StarDB()

   def get_air_mass(self, name):
      """
      Return the air mass of star known by its name
      """
      # get equatorial coordinates
      q = self._stardb.query(name)
      equat_coords = astropy.coordinates.SkyCoord(
         q["RA"]+" "+q["DEC"],   
         unit=(astropy.units.hour,astropy.units.deg))
      # compute the altaz coords
      local_coords = equat_coords.transform_to(self._frame)
      return local_coords.altaz.secz

def differential_photometry(sources, sigma):
   """
   Perform a differential photometry on a list of sources.
   Stars that are not marked as variable are taken as reference.
   Magnitude prediction is done for stars that are marked as variable.

   Arguments:
   ----------
   sources: a list of Source
   sigma  : the global standard deviation of the image
   """
   # calibration with reference stars
   ref_stars = [src for src in sources if not src.var]
   ref_mag = np.array([src.ref_mag for src in ref_stars],
      dtype=np.float)
   meas_flux = np.array([src.moments[0,0] for src in ref_stars],
      dtype=np.float)
   meas_flux_max = meas_flux.max()
   meas_flux = meas_flux / meas_flux_max   # normalize
   meas_mag = flux2mag(meas_flux)
   ref_am = np.array([src.air_mass for src in ref_stars],
      dtype = np.float)
   calib_law = np.polyfit(ref_am, ref_mag-meas_mag, 1)
   # fitting error
   err = ref_mag-(meas_mag+np.polyval(calib_law, ref_am))
   print("Fitting the extinction law with reference stars:")
   for i, src in enumerate(ref_stars):
      print("error for {} = {:5.3f} mag, snr = {:5.1f} dB"
         .format(src.name,
            np.abs(err[i]), 20*np.log10(src.moments[0,0]/(src.diameter*sigma))))

   # predict the flux for the variable stars
   var_star = [src for src in sources if src.var]
   meas_flux2 = np.array([src.moments[0,0] for src in var_star],
      dtype=np.float) / meas_flux_max  # normalize
   meas_mag2 = flux2mag(meas_flux2)
   meas_am = np.array([src.air_mass for src in var_star],
      dtype=np.float)
   pred_mag2 = meas_mag2 + np.polyval(calib_law, meas_am)
   pred_flux2 = mag2flux(pred_mag2)

   # display the predicted magnitudes for the variable stars
   # with 90% confidence interval
   print("Predicted magnitudes:")
   for i, src in enumerate(var_star):
      ic90 = confidence_interval(
         meas_am[i], src.diameter*sigma/meas_flux_max, 0.1, ref_am)
      low_mag = flux2mag(pred_flux2[i] + ic90)
      high_mag = flux2mag(pred_flux2[i] - ic90)
      print("{} mag = {:5.3f}, IC90% = [{:5.3f}, {:5.3f}]"
         .format(src.name, pred_mag2[i], low_mag, high_mag))

