# -*- coding: utf-8 -*-
"""
Mini star database
- Datas are fetched from simbad
- Persistency using gnu DBM
This a kind of "cache" in front of simbad.

Example:
--------
from mini_star_db import StarDB
db = StarDB()
megrez = db.query("Megrez")
print(megrez["RA"], megrez["DEC"], megrez["FLUX_V"])
"""
import os.path
import codecs
import datetime
import dbm.gnu
import astroquery.simbad as simbad

_DB_FILE = "mini_star_db.gdbm"
_SIMBAD_FIELDS = "flux(B)", "flux(V)", "flux(R)"
_FIELDS        = "RA", "DEC", "FLUX_B", "FLUX_V", "FLUX_R"

def query_simbad(name):
   print("query simbad for", name)
   custom_simbad = simbad.Simbad()
   for f in _SIMBAD_FIELDS:
      custom_simbad.add_votable_fields(f)
   return custom_simbad.query_object(name)

class StarDB:
   """
   The stars are stored in a Gnu DBM database.
   A star is identified by its name.
   An entry in the database is created as:
      name -> hashvalue
   The hashvalue is computed from the name and the current date/time.
   This hashvalue is then used to store the attributes of the star
   (equatorial coordinates, magnitude along the Johnson-Cousins filters)
   in entries created as:
      hashvalue_RA -> value of right ascension (H M S)
      hashvalue_DEC -> value of declination (D M S)
      hashvalue_FLUX_B -> value of B-band magnitude
      hashvalue_FLUX_V -> value of V-band magnitude
      hashvalue_FLUX_R -> value of R-band magnitude
   """
   def __init__(self):
      """
      Initialize the database, creating it if empty
      """
      filename = os.path.join(os.path.dirname(__file__), _DB_FILE)
      self._db = dbm.gnu.open(filename, "c")

   def fkey(self, elem_id, fld):
      """
      Concatenate element id and file name to create a key
      """
      if isinstance(elem_id, bytes):
         return elem_id + codecs.encode("_" + fld)
      else:
         return codecs.encode(elem_id + "_" + fld)

   def query(self, name):
      """
      Query a star by its name, sending a query to simbad if non-existent
      in the database.
      """
      bname = codecs.encode(name)  # string to bytes
      if name in self._db:
         # existing elemen
         elem_id = self._db[bname]
      else:
         # non-existing element, get it from simbad
         q = query_simbad(name)
         if q is None:
            return q  # query failed!
         # then store into database
         d = datetime.datetime.now()
         elem_id = str(hash(name + d.strftime("%Y%m%d%H%M%S")))
         self._db[bname] = elem_id
         for f in _FIELDS:
            self._db[self.fkey(elem_id, f)] = str(q[0][f])
      # get the field values
      return { f: bytes.decode(self._db[self.fkey(elem_id, f)])
               for f in _FIELDS }

   def list_stars(self):
      """
      List the name of all the stars stored in the local data base
      """
      lstars = []
      for k in self._db.keys():
         try:
            # if the value is an int, the key is the name of a star
            v = self._db[k]
            x = int(bytes.decode(v))
            lstars.append(bytes.decode(k))
         except ValueError:
            # this key is not the name of a star
            pass
      lstars.sort()
      return lstars

   def __del__(self):
      """
      Close the database on exit
      """
      self._db.close()
