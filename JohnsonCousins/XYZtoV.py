# -*- coding: utf-8 -*-
"""
Approximate conversion from colorimetric XYZ system to photometric UBV system
"""
import numpy as np
import matplotlib.pyplot as plt


# Load the Johnson V response curve
_JOHNSON_V = np.loadtxt("Johnson_V.txt")

def hgaussian(x, a, mu, sigma_left, sigma_right):
   """
   A piecewise Gaussian function
   """
   xa = x*10  # nm to Angstrom
   sigma = np.zeros(xa.shape)
   sigma[xa < mu]  = sigma_left
   sigma[xa >= mu] = sigma_right
   t = (xa-mu) / sigma
   return a * np.exp(-0.5*t*t)

def XYZ(wl):
   """
   Compute the value of the XYZ response curves at wavelength wl (in nm)
   """
   return np.vstack((
      hgaussian(wl,  1.056, 5998, 379, 310) +
      hgaussian(wl,  0.362, 4420, 160, 267) +
      hgaussian(wl, -0.065, 5011, 204, 262),

      hgaussian(wl,  0.821, 5688, 469, 405) +
      hgaussian(wl,  0.286, 5309, 163, 311),

      hgaussian(wl,  1.217, 4370, 118, 360) +
      hgaussian(wl,  0.681, 4590, 260, 138)
   )).T

def Johnson_V(wl):
   """
   Evaluate the V response curve
   """
   a = np.interp(wl, _JOHNSON_V[:,0], _JOHNSON_V[:,1])/_JOHNSON_V[:,1].max()
   return np.reshape(a, (a.size,1))

if __name__ == "__main__":
   wl = np.arange(450.0, 650.0)
   a = XYZ(wl)
   b = Johnson_V(wl)
   k = np.dot(np.linalg.pinv(a), b)
   k = np.reshape(k, (3,))
   print("k = ", k)
   Vsynth = k[0]*a[:,0] + k[1]*a[:,1] + k[2]*a[:,2]

   plt.figure(1, figsize=(10,7))
   plt.subplot(2,1,1)
   plt.plot(wl, a[:,0], "r", wl, a[:,1], "g", wl, a[:,2], "b")
   plt.grid(True)
   plt.legend(("X", "Y", "Z"))
   plt.title("XYZ to V coefficients: $k_0$={:6.3f}, $k_1$={:6.3f}, $k_2$={:6.3f}".format(k[0], k[1], k[2]))
   plt.subplot(2,1,2)
   plt.plot(wl, b, "g", wl, Vsynth, "k--")
   plt.grid(True)
   plt.xlabel("Wavelength [nm]")
   plt.legend(("V", "$k_0$X+$k_1$Y+$k_2$Z"))
   plt.show()
